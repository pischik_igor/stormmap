package com.itrextest.pischik_igor.stormmap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;

import java.util.List;

/**
 * Created by homealone on 09.11.15.
 */
public class SaveStormTask extends AsyncTask<List<String[]>,Integer,Object> {
    private Activity mActivity;
    private MapsHelper mapsHelper;
    private GoogleMap mGoogleMap;
    private Context mContext;
    DatabaseConnector mDatabaseConnector;
    ProgressDialog progressDialog;



    public SaveStormTask(Activity activity, Context context, GoogleMap googleMap) {
        this.mActivity = activity;
        this.mContext = context;
        this.mGoogleMap = googleMap;
        this.mDatabaseConnector = new DatabaseConnector(mContext);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(mActivity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setTitle("Import data from file to database");
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setMax(100);
        progressDialog.show();

    }

    @Override
    protected void onProgressUpdate(Integer...values) {
        super.onProgressUpdate(values);
        progressDialog.setProgress(values[0]);
    }

    @Override
    protected Object doInBackground(List<String[]>... params) {
        mDatabaseConnector.open();
        mDatabaseConnector.createTable(Integer.parseInt(params[0].get(1)[0]));
        int size = params[0].size();
        for (int rowCount = 0; rowCount<params[0].size(); rowCount++) {
            if (params[0].get(rowCount)[8] != null) {
                ContentValues newStorm = new ContentValues();
                newStorm.put("year", Integer.parseInt(params[0].get(rowCount)[0]));
                newStorm.put("event_type", params[0].get(rowCount)[1]);
                newStorm.put("cz_name", params[0].get(rowCount)[2]);
                newStorm.put("begin_date_time", params[0].get(rowCount)[3]);
                if (params[0].get(rowCount)[4] != null) {
                    newStorm.put("injuries_direct", Integer.parseInt(params[0].get(rowCount)[4]));
                } else {
                    newStorm.put("injuries_direct", (Integer) null);
                }

                if (params[0].get(rowCount)[5] != null) {
                    newStorm.put("death_direct", Integer.parseInt(params[0].get(rowCount)[5]));
                } else {
                    newStorm.put("death_direct", (Integer) null);
                }

                newStorm.put("damage_property", params[0].get(rowCount)[6]);
                newStorm.put("damage_crops", params[0].get(rowCount)[7]);
                if (params[0].get(rowCount)[8] != null) {
                    newStorm.put("begin_lat", Double.parseDouble(params[0].get(rowCount)[8]));
                } else {
                    newStorm.put("begin_lat", (Double) null);
                }

                if (params[0].get(rowCount)[9] != null) {
                    newStorm.put("begin_lon", Double.parseDouble(params[0].get(rowCount)[9]));
                } else {
                    newStorm.put("begin_lon", (Double) null);
                }

                if (params[0].get(rowCount)[10] != null) {
                    newStorm.put("end_lat", Double.parseDouble(params[0].get(rowCount)[10]));
                } else {
                    newStorm.put("end_lat", (Double) null);
                }

                if (params[0].get(rowCount)[11] != null) {
                    newStorm.put("end_lon", Double.parseDouble(params[0].get(rowCount)[11]));

                } else {
                    newStorm.put("end_lon", (Double) null);
                }
                mDatabaseConnector.insertStorm(newStorm);
                publishProgress(rowCount * 100 / size);
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        progressDialog.dismiss();
        mDatabaseConnector.close();
    }
}
