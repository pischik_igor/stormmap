package com.itrextest.pischik_igor.stormmap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;

import java.util.ArrayList;


public class SetStormsToMapTask extends AsyncTask<Cursor, Integer, ArrayList<Storm>> {
    private MapsHelper mMapsHelper;
    private GoogleMap mGoogleMap;
    private Context mContext;
    private CustomClusterManager mClusterManager;
    private DatabaseConnector mDatabaseConnector;
    private ProgressDialog progressDialog;
    private Activity mActivity;

    public SetStormsToMapTask(MapsHelper mMapsHelper, GoogleMap mGoogleMap, Context mContext,
                              CustomClusterManager mClusterManager, DatabaseConnector mDatabaseConnector, Activity activity) {
        this.mMapsHelper = mMapsHelper;
        this.mGoogleMap = mGoogleMap;
        this.mContext = mContext;
        this.mClusterManager = mClusterManager;
        this.mDatabaseConnector = mDatabaseConnector;
        this.mActivity = activity;
    }

    public SetStormsToMapTask() {}

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(mActivity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setTitle("Initialize data, please wait!");
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setMax(100);
        progressDialog.show();

    }

    @Override
    protected void onProgressUpdate(Integer...values) {
        super.onProgressUpdate(values);
        progressDialog.setProgress(values[0]);
    }

    @Override
    protected ArrayList<Storm> doInBackground(Cursor... params) {
        ArrayList<Storm> stormList = new ArrayList<>();
        int size = params[0].getCount();
        while (params[0].moveToNext()) {
            Storm storm = new Storm();
            storm.setBeginLat(params[0].getDouble(params[0].getColumnIndex("begin_lat")));
            storm.setBeginLon(params[0].getDouble(params[0].getColumnIndex("begin_lon")));
            storm.setEndLat(params[0].getDouble(params[0].getColumnIndex("end_lat")));
            storm.setEndLon(params[0].getDouble(params[0].getColumnIndex("end_lon")));
            storm.setCzName(params[0].getString(params[0].getColumnIndex("cz_name")));
            storm.setEventType(params[0].getString(params[0].getColumnIndex("event_type")));
            storm.setDeathDirect(params[0].getInt(params[0].getColumnIndex("death_direct")));
            storm.setBeginDateTime(params[0].getString(params[0].getColumnIndex("begin_date_time")));
            storm.setDamageProperty(params[0].getString(params[0].getColumnIndex("damage_property")));
            storm.setDamageCrops(params[0].getString(params[0].getColumnIndex("damage_crops")));
            int position = params[0].getPosition();
            double begin_lat = storm.getBeginLat();
            double begin_lon = storm.getBeginLon();
            mMapsHelper.addItems(begin_lat, begin_lon, storm);
            publishProgress(position * 100 / size);
        }
        return stormList;
    }

    @Override
    protected void onPostExecute(ArrayList<Storm> stormsList) {
        mDatabaseConnector.close();
        mMapsHelper.setUpClusterer(mGoogleMap);
        progressDialog.dismiss();
        mClusterManager.cluster();
    }
}
