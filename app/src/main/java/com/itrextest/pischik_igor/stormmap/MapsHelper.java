package com.itrextest.pischik_igor.stormmap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;
import android.support.design.widget.Snackbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.algo.NonHierarchicalDistanceBasedAlgorithm;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MapsHelper implements ClusterManager.OnClusterItemInfoWindowClickListener<MarkerItem> {

    private GoogleMap mMap;
    public static CustomClusterManager<MarkerItem> mClusterManager;
    private CustomClusterRenderer mCustomClusterRenderer;
    public Activity activity;
    private DisplayMetrics metrics = new DisplayMetrics();
    public static List<MarkerItem> markerItemsList = new ArrayList<>();
    private HeatmapTileProvider mProvider;
    private TileOverlay mOverlay;
    public static String mMapsLayoutState = "NORMAL";

    public MapsHelper(Activity _activity) {
        this.activity = _activity;
    }

    public void mapDefaultOptions (GoogleMap googleMap) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(38.42, -93.77), 3));
        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
    }

    public void setStormsToMap (GoogleMap googleMap, Cursor storms, Activity context, DatabaseConnector databaseConnector) {
        context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mMap = googleMap;
        mClusterManager = new CustomClusterManager<>(context, googleMap);
        mCustomClusterRenderer = new CustomClusterRenderer(context, mMap,
                mClusterManager);
        mClusterManager.setRenderer(mCustomClusterRenderer);
        mClusterManager.setClusterOnlyVisibleArea(true);
        mClusterManager.setAlgorithm(new VisibleNonHierarchicalDistanceBasedAlgorithm<MarkerItem>(metrics.widthPixels, metrics.heightPixels));
        MapsHelper.markerItemsList.clear();
        MapsHelper.mMapsLayoutState = "NORMAL";
        mClusterManager.clearItems();

        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(context));
        SetStormsToMapTask setStormsToMapTask = new SetStormsToMapTask(this, mMap, context, mClusterManager, databaseConnector, activity);
        setStormsToMapTask.execute(storms);

    }

    public void animateMarker(final GoogleMap googleMap, final Marker marker,
                              final boolean hideMarker, final Storm storm) {

        final Handler handler = new Handler();
        final Double end_lat = new BigDecimal(storm.getEndLat()).setScale(2, RoundingMode.UP).doubleValue();
        final Double end_lon = new BigDecimal(storm.getEndLon()).setScale(2, RoundingMode.UP).doubleValue();
        final long start = SystemClock.uptimeMillis();
        Projection proj = googleMap.getProjection();
        final Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final Interpolator interpolator = new LinearInterpolator();
        Location markerLocBegin = new Location ("begin_loc");
        Location markerLocEnd = new Location("end_loc");
        final double begin_lat = new BigDecimal(storm.getBeginLat()).setScale(2, RoundingMode.UP).doubleValue();
        final double begin_lon = new BigDecimal(storm.getBeginLon()).setScale(2, RoundingMode.UP).doubleValue();
        markerLocBegin.setLatitude(begin_lat);
        markerLocBegin.setLongitude(begin_lon);
        markerLocEnd.setLatitude(end_lat);
        markerLocEnd.setLongitude(end_lon);
        final float duration;
        Float distance = markerLocBegin.distanceTo(markerLocEnd);
        duration = (float)(distance/0.8);


        Log.e("Distance: ", distance.toString());
        Log.e("Begin_loc", markerLocBegin.toString());
        Log.e("End_lat", end_lat.toString());
        Log.e("End_lon", end_lon.toString());

        if (end_lat!=0 & end_lon != 0 & distance!=0.0) {

            handler.post(new Runnable() {
                Runnable runnable = this;

                @Override
                public void run() {
                    setupCameraForAnimation(marker, mMap);
                    FloatingActionButton fabStartAnimation = (FloatingActionButton) activity.findViewById(R.id.fab_start_animations);
                    fabStartAnimation.setVisibility(View.GONE);

                    long elapsed = SystemClock.uptimeMillis() - start;
                    final float[] t = {interpolator.getInterpolation((float) elapsed
                            / duration)};
                    final FloatingActionButton fabStopAnimation = (FloatingActionButton) activity.findViewById(R.id.fab_stop_animations);
                    fabStopAnimation.setVisibility(View.VISIBLE);
                    fabStopAnimation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            handler.removeCallbacks(runnable);
                            fabStopAnimation.setVisibility(View.GONE);
                            FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) activity.findViewById(R.id.multiple_actions);
                            floatingActionsMenu.setVisibility(View.VISIBLE);
                            marker.setPosition(new LatLng(begin_lat, begin_lon));
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(begin_lat, begin_lon), 11));
                        }
                    });

                    if (t[0] < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                        CameraPosition cameraPosition = CameraPosition.builder()
                                .target(marker.getPosition())
                                .zoom(11)
                                .bearing(90)
                                .build();
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),
                                100, null);

                    } else {
                        if (hideMarker) {
                            marker.setVisible(false);
                        } else {
                            marker.setVisible(true);
                        }

                        fabStopAnimation.setVisibility(View.GONE);
                        FloatingActionsMenu floatingActionMenu = (FloatingActionsMenu) activity.findViewById(R.id.multiple_actions);
                        floatingActionMenu.setVisibility(View.VISIBLE);
                    }

                    double lon = t[0] * end_lon + (1 - t[0])
                            * startLatLng.longitude;
                    double lat = t[0] * end_lat + (1 - t[0])
                            * startLatLng.latitude;
                    marker.setPosition(new LatLng(lat, lon));
                }

            });
            /**/
        } else {
            View view = activity.findViewById(R.id.activity_main);
            Snackbar.make(view, "For this storm unknown end coordinates!", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
            FloatingActionButton fabStartAnimation = (FloatingActionButton) activity.findViewById(R.id.fab_start_animations);
            fabStartAnimation.setVisibility(View.GONE);
            FloatingActionsMenu floatingActionMenu = (FloatingActionsMenu) activity.findViewById(R.id.multiple_actions);
            floatingActionMenu.setVisibility(View.VISIBLE);
        }
    }

    protected void setUpClusterer(GoogleMap googleMap) {
        googleMap.setOnCameraChangeListener(mClusterManager);

        googleMap.setOnInfoWindowClickListener(mClusterManager);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                MarkerItem markerItem = mCustomClusterRenderer.getClusterItem(marker);
                if (markerItem != null) {
                /*if (marker != null) {*/
                    final FloatingActionsMenu floatingActionMenu = (FloatingActionsMenu) activity.findViewById(R.id.multiple_actions);
                    floatingActionMenu.setVisibility(View.GONE);
                    final FloatingActionButton fabStartAnimation = (FloatingActionButton) activity.findViewById(R.id.fab_start_animations);
                    fabStartAnimation.setVisibility(View.VISIBLE);
                    final FloatingActionButton fabStopAnimation = (FloatingActionButton) activity.findViewById(R.id.fab_stop_animations);
                    setupCameraForAnimation(marker, mMap);
                    fabStartAnimation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (marker != null) {
                                Storm storm = mCustomClusterRenderer.getClusterItem(marker).getStorm();
                                animateMarker(mMap, marker, false, storm);
                            }
                        }
                    });

                    mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            fabStopAnimation.setVisibility(View.GONE);
                            floatingActionMenu.setVisibility(View.VISIBLE);
                        }
                    });

                }
                return false;
            }
        });
        mMap.setOnCameraChangeListener(mClusterManager);
    }

    public void setupCameraForAnimation(Marker marker, GoogleMap googleMap){
        LatLng mapCenter = marker.getPosition();
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mapCenter, 11));
        final CameraPosition cameraPosition = CameraPosition.builder()
                .target(mapCenter)
                .zoom(11)
                .bearing(90)
                .build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),
                100, null);
    }

    protected void addItems(double begin_lat, double begin_lon, Storm storm) {
        /*double offset = position / 60d;*/
        MarkerItem offsetItem = new MarkerItem(begin_lat, begin_lon, storm);
        MapsHelper.markerItemsList.add(offsetItem);
        mClusterManager.addItem(offsetItem);

    }

    public void addOrRemoveHeatMap(GoogleMap googleMap, String damageOrDeath, Activity mActivity) {
        if (mMapsLayoutState.equals("NORMAL") || !mMapsLayoutState.equals(damageOrDeath)) {
            if (mOverlay!=null)
                mOverlay.remove();
            ArrayList<WeightedLatLng> data = new ArrayList<>();
            data.addAll(readItems(damageOrDeath, mActivity));
            MapsHelper.mClusterManager.clearItems();
            MapsHelper.mClusterManager.cluster();
            if (data.size()>0) {
                mProvider = new HeatmapTileProvider.Builder().weightedData(data)
                        .build();
                mOverlay = googleMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                mMapsLayoutState = damageOrDeath;
            }
        } else {
            mOverlay.remove();
            MapsHelper.mClusterManager.addItems(markerItemsList);
            MapsHelper.mClusterManager.cluster();
            mMapsLayoutState = "NORMAL";
        }
    }

    private ArrayList<WeightedLatLng> readItems(String damageOrDeath, Activity mActivity)  {

        ArrayList<WeightedLatLng> stormList = new ArrayList<>();
        for (int i = 0; i < markerItemsList.size(); i++) {
            MarkerItem markerItem = markerItemsList.get(i);
            double lat = markerItem.getStorm().getBeginLat();
            double lng = markerItem.getStorm().getBeginLon();
            if (damageOrDeath.equals("DEATH")) {
                int death = markerItem.getStorm().getDeathDirect();
                if (death != 0.0) {
                    stormList.add(new WeightedLatLng(new LatLng(lat, lng), death));
                }
            } else {
                Double damage = parseDamage(markerItem.getStorm().getDamageProperty()) +
                        parseDamage(markerItem.getStorm().getDamageCrops());
                if (damage != 0.0) {
                    stormList.add(new WeightedLatLng(new LatLng(lat, lng), damage));
                }
            }
        }
        int size = stormList.size();
        Log.e("", ""+size);
        return stormList;
    }

    public Double parseDamage(String damage) {
        Double result = 0.0;
        if (damage != null) {
            try {
                if (damage.indexOf("K") != -1) {
                    result = Double.parseDouble(damage.replaceAll("K", "")) * 1000;
                } else if (damage.indexOf("M") != -1) {
                    result = Double.parseDouble(damage.replaceAll("M", "")) * 1000000;
                } else {
                    result = Double.parseDouble(damage);
                }
            } catch (NumberFormatException e) {
                return 0.0;
            }
        }
        return result;
    }

    @Override
    public void onClusterItemInfoWindowClick(MarkerItem markerItem) {

    }

    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private View view;
        private Context mContext;

        public CustomInfoWindowAdapter(Context context) {
            this.mContext = context;
            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = inflater.inflate(R.layout.custom_info_window,
                    null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            MarkerItem markerItem = mCustomClusterRenderer.getClusterItem(marker);
            if ( markerItem != null) {

                Storm storm =markerItem.getStorm();

                final String name = storm.getCzName();
                final String eventType = storm.getEventType();
                final String beginDate = storm.getBeginDateTime();
                final String death = storm.getDeathDirect().toString();

                final TextView nameDataTextView = (TextView) view.findViewById(R.id.info_window_name_data);

                final TextView typeDataTextView = (TextView) view.findViewById(R.id.info_window_event_type_data);

                final TextView dateDataTextView = (TextView) view.findViewById(R.id.info_window_begin_date_data);

                final TextView deathDataTextView = (TextView) view.findViewById(R.id.info_window_death_direct_data);
                getInfoContents(marker);

                if (name != null) {
                    nameDataTextView.setText(name);
                } else {
                    nameDataTextView.setText("Unknown");
                }
                if (eventType != null) {
                    typeDataTextView.setText(eventType);
                } else {
                    typeDataTextView.setText("Unknown");
                }
                if (beginDate != null) {
                    dateDataTextView.setText(beginDate);
                } else {
                    dateDataTextView.setText("Unknown");
                }
                if (death != null) {
                    deathDataTextView.setText(death);
                } else {
                    deathDataTextView.setText("Unknown");
                }
                return view;
            } else {
                return null;
            }

        }

        @Override
        public View getInfoContents(Marker marker) {
            if (marker != null
                    && marker.isInfoWindowShown()) {
                marker.hideInfoWindow();
                marker.showInfoWindow();
            }
            return null;
        }
    }
}
