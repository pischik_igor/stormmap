package com.itrextest.pischik_igor.stormmap;

import java.util.Date;

/**
 * Created by homealone on 06.11.15.
 */
public class Storm {

    /*private Integer eventId;*/
    private Integer year;
    private String eventType;
    private String czName;
    //    @DatabaseField(columnName = "BEGIN_DATE_TIME")
//    private String beginDateTime;
//    @DatabaseField(columnName = "CZ_TIMEZONE")
//    private String czTimezone;
//    @DatabaseField(columnName = "END_DATE_TIME")
//    private String endDateTime;
    private Integer injuriesDirect;
    /*private Integer injuriesIndirect;*/
    private Integer deathDirect;
    /*private Integer deathIndirect;*/
    private String damageProperty;
    private String damageCrops;
    /*private String torFScale;
    private Double torLength;
    private Double torWidth;*/
   /* private String beginLocation;
    private String endLocation;*/
    private Double beginLat;
    private Double beginLon;
    private Double endLat;
    private Double endLon;
    private String beginDateTime;

    public Storm() {

    }

    public Storm(Integer year, String eventType, String czName, Integer injuriesDirect,
                 Integer deathDirect, String damageProperty, String damageCrops, Double beginLat,
                 Double beginLon, Double endLat, Double endLon, String beginDateTime) {
        /*this.eventId = eventId;*/
        this.year = year;
        this.eventType = eventType;
        this.czName = czName;
        this.injuriesDirect = injuriesDirect;
        /*this.injuriesIndirect = injuriesIndirect;*/
        this.deathDirect = deathDirect;
        /*this.deathIndirect = deathIndirect;*/
        this.damageProperty = damageProperty;
        this.damageCrops = damageCrops;
        /*this.torFScale = torFScale;
        this.torLength = torLength;
        this.torWidth = torWidth;
        this.beginLocation = beginLocation;
        this.endLocation = endLocation;*/
        this.beginLat = beginLat;
        this.beginLon = beginLon;
        this.endLat = endLat;
        this.endLon = endLon;
        this.beginDateTime = beginDateTime;
    }

    /*public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }*/

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getCzName() {
        return czName;
    }

    public void setCzName(String czName) {
        this.czName = czName;
    }

    public Integer getInjuriesDirect() {
        return injuriesDirect;
    }

    public void setInjuriesDirect(Integer injuriesDirect) {
        this.injuriesDirect = injuriesDirect;
    }

    /*public Integer getInjuriesIndirect() {
        return injuriesIndirect;
    }

    public void setInjuriesIndirect(Integer injuriesIndirect) {
        this.injuriesIndirect = injuriesIndirect;
    }*/

    public Integer getDeathDirect() {
        return deathDirect;
    }

    public void setDeathDirect(Integer deathDirect) {
        this.deathDirect = deathDirect;
    }

    /*public Integer getDeathIndirect() {
        return deathIndirect;
    }

    public void setDeathIndirect(Integer deathIndirect) {
        this.deathIndirect = deathIndirect;
    }*/

    public String getDamageProperty() {
        return damageProperty;
    }

    public void setDamageProperty(String damageProperty) {
        this.damageProperty = damageProperty;
    }

    public String getDamageCrops() {
        return damageCrops;
    }

    public void setDamageCrops(String damageCrops) {
        this.damageCrops = damageCrops;
    }

    /*public String getTorFScale() {
        return torFScale;
    }

    public void setTorFScale(String torFScale) {
        this.torFScale = torFScale;
    }

    public Double getTorLength() {
        return torLength;
    }

    public void setTorLength(Double torLength) {
        this.torLength = torLength;
    }

    public Double getTorWidth() {
        return torWidth;
    }

    public void setTorWidth(Double torWidth) {
        this.torWidth = torWidth;
    }

    public String getBeginLocation() {
        return beginLocation;
    }

    public void setBeginLocation(String beginLocation) {
        this.beginLocation = beginLocation;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }*/

    public Double getBeginLat() {
        return beginLat;
    }

    public void setBeginLat(Double beginLat) {
        this.beginLat = beginLat;
    }

    public Double getBeginLon() {
        return beginLon;
    }

    public void setBeginLon(Double beginLon) {
        this.beginLon = beginLon;
    }

    public Double getEndLat() {
        return endLat;
    }

    public void setEndLat(Double endLat) {
        this.endLat = endLat;
    }

    public Double getEndLon() {
        return endLon;
    }

    public void setEndLon(Double endLon) {
        this.endLon = endLon;
    }

    public String getBeginDateTime() {
        return beginDateTime;
    }

    public void setBeginDateTime(String beginDateTime) {
        this.beginDateTime = beginDateTime;
    }
}
