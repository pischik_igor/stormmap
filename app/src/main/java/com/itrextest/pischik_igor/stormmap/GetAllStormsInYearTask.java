package com.itrextest.pischik_igor.stormmap;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;


public class GetAllStormsInYearTask extends AsyncTask<Integer, Object, Cursor> {
    private Activity mActivity;
    private MapsHelper mapsHelper;
    private GoogleMap mGoogleMap;
    private Context mContext;
    DatabaseConnector mDatabaseConnector;

    public GetAllStormsInYearTask(Activity activity, Context context, GoogleMap googleMap) {
        this.mActivity = activity;
        this.mContext = context;
        this.mGoogleMap = googleMap;
        this.mDatabaseConnector = new DatabaseConnector(mContext);
    }

    public GetAllStormsInYearTask(){}


    @Override
    protected Cursor doInBackground(Integer... params) {
        mDatabaseConnector.open();
        return mDatabaseConnector.getAllStormsInYear(params[0]);
    }

    @Override
    protected void onPostExecute(Cursor result) {
        mapsHelper = new MapsHelper(mActivity);
        mapsHelper.setStormsToMap(mGoogleMap, result, mActivity, mDatabaseConnector);
    }
}
