package com.itrextest.pischik_igor.stormmap;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
/*import android.support.design.widget.FloatingActionButton;*/
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.nononsenseapps.filepicker.FilePickerActivity;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static Integer selectedYear;
    private static final int CODE_SD = 0;
    private GoogleMap mMap;
    DatabaseConnector databaseConnector;
    MapsHelper mapsHelper = new MapsHelper(this);
    CsvFileParser csvFileParser;
    Context context;
    private Activity mainActivity;
    private GetAllStormsInYearTask getAllStormsInYearTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        FloatingActionButton startAnimationFab = (FloatingActionButton) findViewById(R.id.fab_start_animations);
        startAnimationFab.setVisibility(startAnimationFab.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
        FloatingActionButton stopAnimationFab = (FloatingActionButton) findViewById(R.id.fab_stop_animations);
        stopAnimationFab.setVisibility(stopAnimationFab.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
        databaseConnector = new DatabaseConnector(this);
        mainActivity = this;
        mMap = setupMap();
        context = getApplicationContext();
        csvFileParser = new CsvFileParser(context);
        getAllStormsInYearTask = new GetAllStormsInYearTask(this, context ,mMap);
        final View openFileButton = findViewById(R.id.open_file_button);
        final View setHeatMapDeathButton = findViewById(R.id.change_map_to_death_type);
        final View setHeatMapDamageButton = findViewById(R.id.change_map_to_damage_type);

        openFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        FilePickerActivity.class);
                intent.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
                startActivityForResult(intent, CODE_SD);
            }
        });

        setHeatMapDeathButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapsHelper.addOrRemoveHeatMap(mMap, "DEATH", mainActivity);
            }
        });

        setHeatMapDamageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapsHelper.addOrRemoveHeatMap(mMap,"DAMAGE", mainActivity);
            }
        });

        final Spinner spinner = (Spinner) findViewById(R.id.choose_year_spinner);
        databaseConnector.open();
        Cursor tablenames = databaseConnector.getAllTablesName();
        ArrayList<Integer> yearList = new ArrayList<>();
        while (tablenames.moveToNext()) {
            String name = tablenames.getString(tablenames.getColumnIndex("name"));
            yearList.add(Integer.parseInt(name.substring(4,8)));
        }
        databaseConnector.close();
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this, R.layout.spinner_row, yearList);
        adapter.setDropDownViewResource(R.layout.spinner_row_dropdown);
        spinner.setAdapter(adapter);



        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedYear = Integer.parseInt(spinner.getSelectedItem().toString());
                mMap.clear();
                getAllStormsInYearTask = new GetAllStormsInYearTask(mainActivity, context ,mMap);
                getAllStormsInYearTask.execute(selectedYear);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        List<String[]> rows;
        if (requestCode == CODE_SD && resultCode == Activity.RESULT_OK) {
            if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)) {
                // For JellyBean and above
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = data.getClipData();
                    if (clip != null) {
                        for (int i = 0; i < clip.getItemCount(); i++) {
                            Uri uri = clip.getItemAt(i).getUri();
                            rows = csvFileParser.csvParse(uri.getPath());
                            SaveStormTask saveStormTask = new SaveStormTask(mainActivity, context, mMap);
                            saveStormTask.execute(rows);
                        }
                    }
                    // For Ice Cream Sandwich
                } else {
                    ArrayList<String> paths = data.getStringArrayListExtra
                            (FilePickerActivity.EXTRA_PATHS);
                    if (paths != null) {
                        for (String path: paths) {
                            Uri uri = Uri.parse(path);
                            rows = csvFileParser.csvParse(uri.getPath());
                            SaveStormTask saveStormTask = new SaveStormTask(mainActivity, context, mMap);
                            saveStormTask.execute(rows);
                        }
                    }
                }
            } else {
                Uri uri = data.getData();
                rows = csvFileParser.csvParse(uri.getPath());
                SaveStormTask saveStormTask = new SaveStormTask(mainActivity, context, mMap);
                saveStormTask.execute(rows);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private GoogleMap setupMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return mapFragment.getMap();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapsHelper.mapDefaultOptions(googleMap);
    }
}


