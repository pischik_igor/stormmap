package com.itrextest.pischik_igor.stormmap;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;


public class CustomClusterRenderer extends CustomBaseClusterRenderer<MarkerItem> {
    private final IconGenerator mClusterIconGenerator;
    private final Context mContext;
    public CustomClusterRenderer(Context context, GoogleMap map, CustomClusterManager clusterManager) {
        super(context, map, clusterManager);
        mClusterIconGenerator = new IconGenerator(context);
        mContext = context;
    }

    @Override
    protected void onBeforeClusterItemRendered(MarkerItem item, MarkerOptions markerOptions) {
        Storm storm = item.getStorm();
        BitmapDescriptor markerDescriptor;
        if (storm.getEventType().equalsIgnoreCase("Tornado")) {
            markerDescriptor = getBitmapDescriptor(mContext, R.drawable.tornadosmall);
        } else {
            markerDescriptor = getBitmapDescriptor(mContext, R.drawable.storm);
        }
        markerOptions.icon(markerDescriptor);
    }

    @Override
    protected void onClusterItemRendered(MarkerItem clusterItem, Marker marker) {
        super.onClusterItemRendered(clusterItem, marker);
    }

    private BitmapDescriptor getBitmapDescriptor(Context context, int resourceId) {
        Drawable vectorDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            vectorDrawable = context.getDrawable(resourceId);
        }
        int h = ((int) convertDpToPixel(42, context));
        int w = ((int) convertDpToPixel(25, context));
        vectorDrawable.setBounds(0, 0, w, h);
        Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bm);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bm);
    }

    private float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }
}
