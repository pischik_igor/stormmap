package com.itrextest.pischik_igor.stormmap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * NEED REFACTOR MAYBE DELETE
 */
public class DatabaseConnector {
    private static final String DATABASE_NAME = "StormsData";
    private SQLiteDatabase database;
    private DatabaseOpenHelper databaseOpenHelper;
    private static final String DATABASE_PATH = String.format("//data//data//com.itrextest.pischik_igor.stormmap//databases//");
    private Context context;
    private String[] DEFAULT_TABLES_NAMES;


    public DatabaseConnector(Context context) {
        this.context = context;
        DEFAULT_TABLES_NAMES = context.getResources().getStringArray(R.array.yearlist);
        databaseOpenHelper = new DatabaseOpenHelper(this.context, DATABASE_NAME, null, 1);
    }

    public void open() throws SQLException {
        database = databaseOpenHelper.getWritableDatabase();
    }

    public void close() {
        if (database != null)
            database.close();
    }

    public void createTable(int year) {
        String createQuery = "CREATE TABLE IF NOT EXISTS year" + year +
                "(_id integer primary key autoincrement," +
                "year integer, event_type TEXT, cz_name TEXT, begin_date_time datetime, " +
                "injuries_direct integer, death_direct integer, " +
                "damage_property TEXT, damage_crops TEXT," +
                "begin_lat double precision, begin_lon double precision," +
                " end_lat double precision, end_lon double precision);";
        database.execSQL(createQuery);
    }

    public Cursor getAllTablesName() {
        Cursor c = database.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name!='android_metadata' AND name!='sqlite_sequence' order by name", null);
        return c;
    }

    public long insertStorm(ContentValues storm) {
        long rowId = database.insert("year"+storm.get("year"), null, storm);
        return rowId;
    }

    public Cursor getAllStormsInYear(int year) {
        return database.query("year"+year, null ,null, null, null, null, null);
    }

    private class DatabaseOpenHelper extends SQLiteOpenHelper {

        public DatabaseOpenHelper (Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
            /*database = getWritableDatabase();*/
            createDataBase(context);
            //open();
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            /*for (String DEFAULT_TABLES_NAME : DEFAULT_TABLES_NAMES) {
                String createQuery = "CREATE TABLE year" + DEFAULT_TABLES_NAME +
                        "(_id integer primary key autoincrement," +
                        "year integer, event_type TEXT, cz_name TEXT, begin_date_time datetime, " +
                        "injuries_direct integer, death_direct integer, " +
                        "damage_property TEXT, damage_crops TEXT," +
                        "begin_lat double precision, begin_lon double precision," +
                        " end_lat double precision, end_lon double precision);";
                db.execSQL(createQuery);
            }*/
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }



        private void copyDataBase(Context context) throws IOException {
            InputStream externalDbStream = context.getAssets().open(DATABASE_NAME);
            String outFileName = DATABASE_PATH + DATABASE_NAME;
            OutputStream localDbStream = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int bytesRead;

            while ((bytesRead = externalDbStream.read(buffer)) > 0) {
                localDbStream.write(buffer, 0, bytesRead);
            }

            localDbStream.close();
            externalDbStream.close();
        }

        private boolean checkDataBase() {
            SQLiteDatabase checkDB = null;
            try {
                String path = DATABASE_PATH + DATABASE_NAME;
                checkDB = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READONLY);
            } catch (android.database.SQLException e) {
                Log.e(this.getClass().toString(), "Error while checking db");
            }
            if (checkDB != null) {
                checkDB.close();
            }
            return checkDB != null;
        }

        public void createDataBase(Context context){
            boolean dbExist = checkDataBase();
            if (!dbExist) {
                this.getReadableDatabase();
                try {
                    copyDataBase(context);
                } catch (IOException e) {
                    Log.e(this.getClass().toString(), "Copying error");
                    throw new Error("Error copying database");
                }
            } else {
                Log.i(this.getClass().toString(), "Database already exists");
            }
        }
    }
}
