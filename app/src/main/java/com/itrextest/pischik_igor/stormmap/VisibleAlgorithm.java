package com.itrextest.pischik_igor.stormmap;

import com.google.android.gms.maps.model.VisibleRegion;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.algo.Algorithm;


public interface VisibleAlgorithm<T extends ClusterItem> extends Algorithm<T> {
    void setVisibleRegion(VisibleRegion visibleRegion);
}
