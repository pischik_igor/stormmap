package com.itrextest.pischik_igor.stormmap;

import android.content.Context;

import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class CsvFileParser {
    private Context mContext;

    public CsvFileParser(Context context) {
        this.mContext = context;
    }

    public List<String[]> csvParse(String pathToFile) {
        CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.getFormat().setLineSeparator("\n");
        parserSettings.selectFields("YEAR", "EVENT_TYPE", "CZ_NAME", "BEGIN_DATE_TIME",
                "INJURIES_DIRECT", "DEATHS_DIRECT", "DAMAGE_PROPERTY", "DAMAGE_CROPS", "BEGIN_LAT",
                "BEGIN_LON", "END_LAT", "END_LON");
        return parseWithSettings(parserSettings, pathToFile);
    }

    private List<String[]> parseWithSettings(CsvParserSettings parserSettings, String pathToFile) {
        RowListProcessor rowListProcessor = new RowListProcessor();
        parserSettings.setRowProcessor(rowListProcessor);
        parserSettings.setHeaderExtractionEnabled(true);

        CsvParser csvParser = new CsvParser(parserSettings);

        try {
            csvParser.parse(new FileReader(new File(pathToFile)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return rowListProcessor.getRows();
    }
}
