package com.itrextest.pischik_igor.stormmap;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MarkerItem implements ClusterItem {
    private final LatLng mPosition;
    private final Storm mStorm;
    public MarkerItem(double begin_lat, double begin_lon, Storm storm) {
        this.mStorm = storm;
        this.mPosition = new LatLng(begin_lat,begin_lon);
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public Storm getStorm() {
        return mStorm;
    }
}
